<?php

class ModHelloWorldHelper
{

    public static function getHello($params)
    {
        // Obtain a database connection
        $db = JFactory::getDbo();
// Retrieve the shout
        $query = $db->getQuery(true)
            ->select($db->quoteName('hello'))
            ->from($db->quoteName('#__helloworld'))
            ->where('id = ' . $db->Quote($params));
// Prepare the query
        $db->setQuery($query);
// Load the row.
        $result = $db->loadResult();
// Return the Hello
        return $result;
    }

}

